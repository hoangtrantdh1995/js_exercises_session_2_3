// start Ex1
function totalSalary() {
  var oneDaySalary = document.getElementById("one_day_salary").value;
  var numberOfWorkingDays = document.getElementById(
    "number_of_working_days"
  ).value;
  console.log({ oneDaySalary, numberOfWorkingDays });

  var resultTotalSalary = oneDaySalary * numberOfWorkingDays;
  console.log('resultTotalSalary: ', resultTotalSalary);

  var resultTotalSalary = `${new Intl.NumberFormat('VN', { style: 'currency', currency: 'VND' }).format(resultTotalSalary)}`;

  document.getElementById("result1").innerHTML = `<p>Tổng lương: ${resultTotalSalary}</p > `;
  result1.style.background = "#00FFFF"
}
// // end Ex1

// start Ex2
function averageValue() {
  var numberOne = document.getElementById("R1").value;
  var numberTwo = document.getElementById("R2").value;
  var numberThree = document.getElementById("R3").value;
  var numberFour = document.getElementById("R4").value;
  var numberFive = document.getElementById("R5").value;
  console.log({ numberOne, numberTwo, numberThree, numberFour, numberFive });

  var averageValueResult = ((numberOne * 1) + (numberTwo * 1) + (numberThree * 1) + (numberFour * 1) + (numberFive * 1)) / 5;
  console.log('averageValueResult : ', averageValueResult);

  var averageValueResult = `${new Intl.NumberFormat('en-US', { style: "decimal", }).format(averageValueResult)}`;

  document.getElementById("result2").innerHTML = `<p>Giá trị trung bình: ${averageValueResult}</p>`
  result2.style.background = "#00FFFF"
}
// end Ex2


// start Ex3
function currencyExchange() {
  var currencyVND = 23500;
  var currencyUSD = document.getElementById("currency_USD").value;
  console.log({ currencyVND, currencyUSD });

  var conversionResult = currencyVND * currencyUSD;
  console.log('conversionResult: ', conversionResult);


  var conversionResult = `${new Intl.NumberFormat('VN', { style: 'currency', currency: 'VND' }).format(conversionResult)}`;

  document.getElementById("result3").innerHTML = `<p>Quy đổi tiền VND: ${conversionResult}</p>`;
  result3.style.background = "#00FFFF"
}
// end Ex3

// start Ex4
function total() {
  var rectangleLength = document.getElementById("length_of_rectangle").value;
  var rectangleWidth = document.getElementById("rectangle_width").value;

  var rectangleArea = rectangleLength * rectangleWidth;

  var rectanglePerimeter = (rectangleLength * 1 + rectangleWidth * 1) * 2;
  console.log({ rectangleLength, rectangleWidth, rectangleArea, rectanglePerimeter });

  var rectangleArea = `${new Intl.NumberFormat('en-US', { style: "decimal", }).format(rectangleArea)}`;

  var rectanglePerimeter = `${new Intl.NumberFormat('en-US', { style: "decimal", }).format(rectanglePerimeter)}`;

  document.getElementById("result4").innerHTML = `<p>Diện tích hình chữ nhật: ${rectangleArea}</p>
  <p>Chu vi hình chữ nhật: ${rectanglePerimeter}</p>
  `;
  result4.style.background = "#00FFFF"
}
// end Ex4

// start Ex5
function sum() {
  var twoNumbers = document.getElementById("two_number").value;
  var units = Math.floor(twoNumbers % 10);
  var dozens = Math.floor(twoNumbers / 10);
  var totalTwoDigits = units + dozens;

  console.log({ twoNumbers, units, dozens, totalTwoDigits });

  document.getElementById("result5").innerHTML = `<p>Tổng 2 ký số: ${totalTwoDigits}</p>`

  result5.style.background = "#00FFFF"
}